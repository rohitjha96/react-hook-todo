import React, { useState} from 'react';
import Header from './Header';
import AddToDo from './AddToDo';
import ToDoList from './ToDoList';
import './App.css';

export const ToDoContext = React.createContext();
function App() {
    let [state , updateState] = useState({ 
        addToDo :'',
        todos :[
            {id:Math.floor(Math.random()*10000),title:'todo1',completed:false},
            {id:Math.floor(Math.random()*10000),title:'todo2',completed:false},
            {id:Math.floor(Math.random()*10000),title:'todo3',completed:true}
        ]
     });
   
    
        return (
            <div className='container'>
                <Header/>
                <ToDoContext.Provider value={{state,updateState}}>
                <AddToDo/>
                <ToDoList/>
                </ToDoContext.Provider>
            </div >
         );
    }

 
export default App;