import React, { useContext } from "react";
import ToDo from "./todo";
import {ToDoContext} from "./App";
function ToDoList() {

const toDoContext =  useContext(ToDoContext);
  return toDoContext.state.todos.map(todo => (
    <ToDo
      key={todo.id}
      todo={todo}
    />
  ));
}

export default ToDoList;
