/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext } from "react";
import { ToDoContext } from "./App";
function AddToDo() {
  const toDoContext = useContext(ToDoContext);
  const add = () => {
    let tempToDos = [...toDoContext.state.todos];
    let newTodo = {
      id: Math.floor(Math.random() * 10000),
      title: toDoContext.state.addToDo,
      completed: false
    };
    tempToDos.push(newTodo);
    toDoContext.updateState(prevState => {
      return { ...prevState, todos: tempToDos, addToDo: "" };
    });
  };
  return (
    <div className="addToDo">
      <input
        value={toDoContext.state.addToDo}
        onChange={e => {
          e.persist();
          toDoContext.updateState(prevState => {
            return { ...prevState, addToDo: e.target.value };
          });
        }}
      />
      <button className="btn btn-primary btn-sm m3" onClick={() => add()}>
        Add
      </button>
    </div>
  );
}

export default React.memo(AddToDo);
