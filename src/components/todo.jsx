import React, { useContext } from "react";

import { ToDoContext } from "./App";
function ToDo(props) {

const toDoContext = useContext(ToDoContext);
  const handleCheckbox = id => {
    let tempTodo = [...toDoContext.state.todos],
      index = 0;
    for (let i = 0; i < tempTodo.length; i++) {
      if (id === tempTodo[i].id) {
        index = i;
        break;
      }
    }
    tempTodo[index].completed = !tempTodo[index].completed;
    toDoContext.updateState(prevState => {
      return { ...prevState, todos: tempTodo };
    });
  };
  const handleDelete = id => {
    let tempTodo = toDoContext.state.todos.filter(todo => todo.id !== id );
    toDoContext.updateState(prevState => {
      return { ...prevState, todos: tempTodo };
    });
  };
  const titleStyle = () => {
    let a = "";
    if (props.todo.completed === true) {
      a = "line-through";
    } else {
      a = "none";
    }
    return { "textDecoration": a };
  };

  return (
    <div style={{ display: "flex" }}>
      <input
        checked={props.todo.completed}
        type="checkbox"
        onChange={() => handleCheckbox(props.todo.id)}
      />
      <p style={titleStyle()}>{props.todo.title}</p>
      <button
        className="btn btn-warning btn-sm"
        onClick={() => handleDelete(props.todo.id)}
      >
        X
      </button>
    </div>
  );
}

export default ToDo;
